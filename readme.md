# Maker Series: Dan Mall
Design Deliverables in a Post-Comp Era  
Dan Mall - Founder, SuperFriendly  
October 30, 2014

These are some notes that I took from the SparkBox Build Right workshop with Dan Mall


Collaborative notes: http://bit.ly/ZMZ6uA


Most teams in our industry use an outdated assmembly line method.


A framework (not a process)


Developers are always at the tail end of a project, trying to "catch up" to the project that is usually already over budget and over time.


In waterfall, if you change something you have to go back and re-do everything.

In agile, it allows you to be more flexible.

Waterfall has led itself to be more of a race rather than a framework or process.


## Moving to Agile

Instead of complete handoffs from one person to another, create overlap between teams/people in the project. This will allow everyone to be involved with the process.

### Approporate Tapering

Everyone is involved in the beginning. And each team can then taper off when handng off to other teams.

### Divergent thinking

After a kickoff meeting, each member can diverge off with the particular elements that need to be contributed to the project.


## Comps

There has to be a better way.

### Plan

Research. DO A LOT OF RESEARCH!

Lo-fi research.

Ask users about their process.

Hypothesize.

### Inventory

#### Content Inventory
*The Elements of Content Strategy* - Page 52

For every page of the site either Add, Edit, or Remove content.

#### Performance Budget
A performance budget will define how fast a site will be

Tool: Web page test

Look at competitors to see how fast their sites will load.


#### Pattern Inventory
Look at Pattern Lab

Atomic design


Instead of using Pattern Lab out of the box, break down the elements that you will need for the site. In Dan's case, they used spreadsheets to break down the elements and then using those element within PL.

#### Visual Inventory

Design Conversations

Using Keynote

E.G. - Client wants the site to be playful

Go to a "playful" site and then add notes to the example. Add notes in the slide. Maybe even add the client logo and then ask the client if that is what they are kinda looking for.

Create slides to create conversation.

Can even add in some recommendations to the end of the inventory list with some of the different elements.

The highest fidelity in the quickest amount of time.  
All deliverables lead to conversations.  
Replace "sign off" with "go on".  
Keep going in a "direction" instead of doing final approvals.


### Sketching

Draw some elements out on paper.

Book: Responsive Design Workflow.


#### Linear Design

List out the elements of the site.  
Then, block them out in a wireframe-esque grey boxes. It's easier to make grey boxes responsive.

Grantland website used grey boxes.

#### Element Collage

Design elements that can be based from items or keywords that maybe coming from the client.

Turn phrases into visual hooks.

Not full on layouts but just smaller elements that could be used.

You want your clients to give you feedback on art direction, not design.

Getting the right feedback about typography

Talk about the objective parts of the typography instead of "do you like this one or the other one?"  
Talk about *elements* of the design and the typography.  
Make design conversations about the *objective* and not the *subjective*.

Tools: Typecast

Show hover states and transitional elements. That way it looks like canvas and not a "layout".

Instead of going vertically, make them super SUPER wide (horizontal).

Give variations of the same element.

Design = Rendering intent.

Inventing on Principle - Bret Victor Video

#### Protoypes

All prototypes should take an hour or less. If they take longer than that, then scrap it and move on.

Write problem and solution statements in your prototypes.

Each prototype does one thing and one thing only...but very well.

Then test and re-iterate each prototype.

Each prototype should have basic elements:  
E.G.:  
Prototype 1 = Basic div  
Prototype 2 = Div with button  
Prototype 3 = Div with button that hovers  
Prototype 4 = Div with button that hovers and has a link

#### Assemble

Put it all together.

*Mise en Place*


*RWD is a reminder of web standards.*


## Tools

Little Snapper
Organized by tag - ones that you'll remember

Keynote  
Make collages and elements in keynote. Hell, even write your contracts and proposals in it!

## Presenting Designs

Start with a Visual Inventory with recommendations

Then maybe sketch out some elements/pages

Move into some possible comps - more focusing on goals instead of elements of the type and so on and so forth.

#### Visual Flow
Once you have an idea on some of the aesthetics, work on a flow deliverable.
Basically, what page is going to go to where, how is it going to flow within the site. Can even hammer out content elements.  
This can also transition into simple linear designs.
